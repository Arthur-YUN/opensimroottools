# OpenSimRoot Tools - Copyright (C) - 2019 - Ernst Schäfer
# All rights reserved.
#
# This file is part of OpenSimRoot Tools.
#
# OpenSimRoot Tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# OpenSimRoot Tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from tkinter import filedialog
import glob
from xml.etree import ElementTree as ET
import tempfile
from copy import deepcopy
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname( __file__ ), os.pardir)))
from FunctionsXML import *


# Objects of this class are containers that hold the visual elements in the parameter section of the GUI and all the related objects needed for those. They also link to the corresponding elements in the etree
class ContainerBase:
	def __init__(self, xmlElement, fileName, rClass):
		# List containing widgets
		self.widgets = []
		# Dictionary containing variables and the widgets they are linked to
		self.variables = {}
		# Etree element that is linked to this container
		self.XMLElement = xmlElement
		# The template and root class corresponding to this container
		self.file = fileName
		self.rootClass = rClass
		# Position in the GUI
		self.GUIPosition = xmlElement.get("GUIPOSITION")
		if self.GUIPosition == None:
			self.GUIPosition = -1
			print("Warning, no GUIPOSITION found for ", self)
		self.GUIPosition = int(self.GUIPosition)
		self.typeVar = StringVar()
		self.conditionalOn = []
		if "GUIConditionalOn" in xmlElement.attrib:
			self.conditionalOn = xmlElement.get("GUIConditionalOn").split(';')
		self.expertModeOnly = False
		if "GUIExpertMode" in xmlElement.attrib:
			self.expertModeOnly = True
		
	def __str__(self):
		return str(self.XMLElement) + " " + self.file + " " + self.rootClass
			
	def MakeVisible(self, n): # Makes the elements of this container visible in the GUI
		if self.expertModeOnly and not expertMode.get():
			self.MakeInvisible()
			return
		if len(self.conditionalOn) > 0:
			for cond in self.conditionalOn:
				if not any(cond in templ for templ in currentTemplates):
					self.MakeInvisible()
					return
		for i, widget in enumerate(self.widgets):
			if not expertMode.get() and (i == 1 or (self.typeVar.get() == "SimulaTable" and (i == 4 or i == 5 or i == 6))):
				continue
			widget.grid(column = i, row = n, sticky=(N, W, E, S), padx = 2, pady = 1)
	def MakeInvisible(self): # Hides the elements of this container from the GUI
		for widget in self.widgets:
			widget.grid_remove()
	def PrepareForCopyingValues(self, sourceElt):
		newElement = self
		if not self.typeVar.get() == sourceElt.tag:
			self.PrepareForTypeChange(sourceElt.tag)
			newElement = AddElementToParameterDictionary(self.XMLElement, self.file, self.rootClass)
			self.MakeInvisible()
			parameterContainers[:] = [con for con in parameterContainers if (not con == self)]
		for attribute in sourceElt.attrib:
			newElement.XMLElement.set(attribute, sourceElt.get(attribute))
		return newElement

# Objects of this class contain labels that convey simple information the GUI and aren't part of the final XML			
class LabelContainer(ContainerBase):
	def __init__(self, xmlElement, fileName, rClass):
		ContainerBase.__init__(self, xmlElement, fileName, rClass)
		self.labelText = xmlElement.get("GUITEXT").replace("#ROOTCLASS#", rClass)
		self.labelWidget = ttk.Label(parameterFrame, text = self.labelText, width = 25, font='Helvetica 12 bold')
		self.typeVar.set("Label")
		self.widgets = [self.labelWidget]
		for widget in self.widgets:
			widget.bind("<MouseWheel>", ScrollParameter)
			widget.bind("<ButtonPress-4>", ScrollParameterDown)
			widget.bind("<ButtonPress-5>", ScrollParameterUp)
	def MakeVisible(self, n):
		self.widgets[0].grid(column = 0, row = n, sticky=(N, W, E, S), padx = 2, pady = 1, columnspan = 4)
	
# Objects of this class contain elements with choices in the text part of the tag, such as SimulaConstants
class ConstantContainer(ContainerBase):
	def __init__(self, xmlElement, fileName, rClass):
		ContainerBase.__init__(self, xmlElement, fileName, rClass)
		self.typeVar.set(xmlElement.tag)
		self.labelText = xmlElement.get("name")
		if "GUITEXT" in xmlElement.attrib:
			self.labelText = xmlElement.get("GUITEXT")
		self.labelWidget = ttk.Label(parameterFrame, text = self.labelText, width = 25)
		self.typeWidget = ttk.Combobox(parameterFrame, textvariable = self.typeVar, state = "readonly", values = ["SimulaConstant", "SimulaTable", "SimulaStochastic", "SimulaDerivative", "SimulaVariable"], width = 14)
		self.typeWidget.bind('<<ComboboxSelected>>', ChangeElementTrigger)
		self.unitVar = StringVar()
		self.unitVar.set(xmlElement.get("unit"))
		if self.unitVar.get() == None:
			self.unitVar.set("None")
		self.unitWidget = ttk.Label(parameterFrame, text = self.unitVar.get(), width = 14)
		self.constVar = StringVar()
		self.constVar.set(xmlElement.text.split('#')[0])
		if xmlElement.get("name") == "rootClassID":
			self.constVar.set(rootClasses.index(self.rootClass) + 1)
		self.entryWidget = ttk.Entry(parameterFrame, textvariable = self.constVar, width = 25)
		if xmlElement.text.count('#') > 3:
			self.entryWidget = ttk.Combobox(parameterFrame, textvariable = self.constVar, state = "readonly", values = xmlElement.text.split('#')[2:-1], width = 25)
		if xmlElement.get("type") == "bool":
			self.typeWidget = ttk.Label(parameterFrame, text = self.typeVar.get(), width = 14)
			self.entryWidget = ttk.Checkbutton(parameterFrame, variable = self.constVar)
		if xmlElement.get("type") == "string" or xmlElement.get("type") == "int":
			self.typeWidget = ttk.Label(parameterFrame, text = self.typeVar.get(), width = 14)
		if xmlElement.get("name") == "run":
			self.entryWidget = ttk.Checkbutton(parameterFrame, command = ShowSwitchChange, variable = self.constVar)
		if "GUITEXT" in xmlElement.attrib:
			self.labelText = xmlElement.get("GUITEXT")
		self.widgets = [self.labelWidget, self.typeWidget, self.entryWidget, self.unitWidget]
		for widget in self.widgets:
			widget.bind("<MouseWheel>", ScrollParameter)
			widget.bind("<ButtonPress-4>", ScrollParameterDown)
			widget.bind("<ButtonPress-5>", ScrollParameterUp)
	def PrepareForTypeChange(self, goal):
		if goal == "SimulaTable":
			self.XMLElement.set("name_column2", self.labelText)
			self.XMLElement.set("unit_column2", self.unitVar.get())
			self.XMLElement.set("name_column1", "time since creation")
			self.XMLElement.set("unit_column1", "day")
			self.XMLElement.text = "0 0 100 0#"
			self.XMLElement.attrib.pop("name", None)
			self.XMLElement.attrib.pop("unit", None)
			self.XMLElement.tag = "SimulaTable"
			return
		if goal == "SimulaStochastic":
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.set("distribution", "uniform#")
			self.XMLElement.set("minimum", "-0.05#")
			self.XMLElement.set("maximum", "0.05#")
			self.XMLElement.text = ""
			self.XMLElement.tag = "SimulaStochastic"
			return
		if goal == "SimulaDerivative":
			self.XMLElement.set("function", "#FUNCTION#Choose a function#")
			self.XMLElement.text = ""
			self.XMLElement.tag = "SimulaDerivative"
			return
		if goal == "SimulaVariable":
			self.XMLElement.set("function", "#FUNCTION#Choose a function#")
			self.XMLElement.set("integrationFunction", "#FUNCTION#Choose an integration function#")
			self.XMLElement.text = ""
			self.XMLElement.tag = "SimulaVariable"
			return
		print("Error: Trying to change to unknown tag type.", self.labelText, goal)
	def CopyValuesIntoContainer(self, sourceElt):
		if "unit" in sourceElt.attrib:
			self.unitVar.set(sourceElt.get("unit"))
		self.constVar.set(sourceElt.text.strip())
	def SetPlaceHolder(self, entry):
		self.constVar.set(entry)
		if self.XMLElement.get("type") == "bool":
			self.constVar.set("0")
	
# Objects of this class contain SimulaTable elements
class TableContainer(ContainerBase):
	def __init__(self, xmlElement, fileName, rClass):
		ContainerBase.__init__(self, xmlElement, fileName, rClass)
		ContainerBase.__init__(self, xmlElement, fileName, rClass)
		self.typeVar.set(xmlElement.tag)
		self.labelText = xmlElement.get("name_column2")
		if "GUITEXT" in xmlElement.attrib:
			self.labelText = xmlElement.get("GUITEXT")
		self.labelWidget = ttk.Label(parameterFrame, text = self.labelText, width = 25)
		self.typeWidget = ttk.Combobox(parameterFrame, textvariable = self.typeVar, state = "readonly", values = ["SimulaConstant", "SimulaTable", "SimulaStochastic", "SimulaDerivative", "SimulaVariable"], width = 14)
		self.typeWidget.bind('<<ComboboxSelected>>', ChangeElementTrigger)
		self.unitVar = StringVar()
		self.unitVar.set(xmlElement.get("unit_column2"))
		if self.unitVar.get() == None:
			self.unitVar.set("None")
		self.unitWidget = ttk.Label(parameterFrame, text = self.unitVar.get(), width = 14)
		self.constVar = StringVar()
		self.constVar.set(xmlElement.text.split('#')[0])
		self.entryWidget = ttk.Entry(parameterFrame, textvariable = self.constVar, width = 25)
		self.colVar = StringVar()
		self.colVar.set(xmlElement.get("name_column1"))
		self.colWidget = ttk.Combobox(parameterFrame, textvariable = self.colVar, state = "readonly", values = ["time since creation", "depth"], width = 5)
		self.variables["name_column1"] = self.colVar
		self.interpolationVar = StringVar()
		self.interpolationVar.set(xmlElement.get("interpolationFunction"))
		self.interpolationWidget = ttk.Combobox(parameterFrame, textvariable = self.interpolationVar, state = "readonly", values = ["linear", "step"], width = 6)
		self.interpolationLabel = ttk.Label(parameterFrame, text = "Interpolation Function", width = 20)
		self.variables["interpolationFunction"] = self.interpolationVar
		self.widgets = [self.labelWidget, self.typeWidget, self.entryWidget, self.unitWidget, self.colWidget, self.interpolationLabel, self.interpolationWidget]
		for widget in self.widgets:
			widget.bind("<MouseWheel>", ScrollParameter)
			widget.bind("<ButtonPress-4>", ScrollParameterDown)
			widget.bind("<ButtonPress-5>", ScrollParameterUp)
	def PrepareForTypeChange(self, goal):
		if goal == "SimulaConstant":
			self.XMLElement.set("name", self.labelText)
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.text = "0#"
			self.XMLElement.attrib.pop("name_column2", None)
			self.XMLElement.attrib.pop("unit_column2", None)
			self.XMLElement.attrib.pop("name_column1", None)
			self.XMLElement.attrib.pop("unit_column1", None)
			self.XMLElement.tag = "SimulaConstant"
			return
		if goal == "SimulaStochastic":
			self.XMLElement.set("name", self.labelText)
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.set("distribution", "uniform#")
			self.XMLElement.set("minimum", "-0.05#")
			self.XMLElement.set("maximum", "0.05#")
			self.XMLElement.text = ""
			self.XMLElement.attrib.pop("name_column2", None)
			self.XMLElement.attrib.pop("unit_column2", None)
			self.XMLElement.attrib.pop("name_column1", None)
			self.XMLElement.attrib.pop("unit_column1", None)
			self.XMLElement.tag = "SimulaStochastic"
			return
		if goal == "SimulaDerivative":
			self.XMLElement.set("name", self.labelText)
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.set("function", "#FUNCTION#Choose a function#")
			self.XMLElement.text = ""
			self.XMLElement.attrib.pop("name_column2", None)
			self.XMLElement.attrib.pop("unit_column2", None)
			self.XMLElement.attrib.pop("name_column1", None)
			self.XMLElement.attrib.pop("unit_column1", None)
			self.XMLElement.tag = "SimulaDerivative"
			return
		if goal == "SimulaVariable":
			self.XMLElement.set("name", self.labelText)
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.set("function", "#FUNCTION#Choose a function#")
			self.XMLElement.set("integrationFunction", "#FUNCTION#Choose an integration function#")
			self.XMLElement.text = ""
			self.XMLElement.attrib.pop("name_column2", None)
			self.XMLElement.attrib.pop("unit_column2", None)
			self.XMLElement.attrib.pop("name_column1", None)
			self.XMLElement.attrib.pop("unit_column1", None)
			self.XMLElement.tag = "SimulaVariable"
			return
		print("Error: Trying to change to unknown tag type.", self.labelText, goal)
	def CopyValuesIntoContainer(self, sourceElt):
		if "unit_column2" in sourceElt.attrib:
			self.unitVar.set(sourceElt.get("unit_column2"))
		if "name_column1" in sourceElt.attrib:
			self.colVar.set(sourceElt.get("name_column1"))
		if "interpolationFunction" in sourceElt.attrib:
			self.interpolationVar.set(sourceElt.get("interpolationFunction"))
		self.constVar.set(sourceElt.text.strip())
	def SetPlaceHolder(self, entry):
		self.constVar.set(entry)
	
# Objects of this class contain SimulaStochastic elements
class StochasticContainer(ContainerBase):
	def __init__(self, xmlElement, fileName, rClass):
		ContainerBase.__init__(self, xmlElement, fileName, rClass)
		self.labelText = xmlElement.get("name")
		if "GUITEXT" in xmlElement.attrib:
			self.labelText = xmlElement.get("GUITEXT")
		self.labelWidget = ttk.Label(parameterFrame, text = self.labelText, width = 25)
		self.typeVar.set(xmlElement.tag)
		self.typeWidget = ttk.Combobox(parameterFrame, textvariable = self.typeVar, state = "readonly", values = ["SimulaConstant", "SimulaTable", "SimulaStochastic", "SimulaDerivative", "SimulaVariable"], width = 14)
		self.typeWidget.bind('<<ComboboxSelected>>', ChangeElementTrigger)
		self.unitVar = StringVar()
		self.unitVar.set(xmlElement.get("unit"))
		if self.unitVar.get() == None:
			self.unitVar.set("None")
		self.unitWidget = ttk.Label(parameterFrame, text = self.unitVar.get())
		self.distributionVar = StringVar()
		self.distributionVar.set(xmlElement.get("distribution").split('#')[0])
		self.distributionWidget = ttk.Combobox(parameterFrame, textvariable = self.distributionVar, state = "readonly", values = possibleDistributions, width = 14)
		self.minVar = StringVar()
		self.minVar.set(xmlElement.get("minimum").split('#')[0])
		self.minWidget = ttk.Entry(parameterFrame, textvariable = self.minVar, width = 5)
		self.minLabel = ttk.Label(parameterFrame, text = "min", width = 3)
		self.maxVar = StringVar()
		self.maxVar.set(xmlElement.get("maximum").split('#')[0])
		self.maxWidget = ttk.Entry(parameterFrame, textvariable = self.maxVar, width = 5)
		self.maxLabel = ttk.Label(parameterFrame, text = "max", width = 3)

		self.meanVar = StringVar()
		if "mean" in xmlElement.attrib:
			self.meanVar.set(xmlElement.get("mean").split('#')[0])
		else:
			self.meanVar.set("")
		self.meanWidget = ttk.Entry(parameterFrame, textvariable = self.meanVar, width = 5)
		self.meanLabel = ttk.Label(parameterFrame, text = "mean", width = 4)
		self.sdVar = StringVar()
		if "stdev" in xmlElement.attrib:
			self.sdVar.set(xmlElement.get("stdev").split('#')[0])
		else:
			self.sdVar.set("")
		self.sdWidget = ttk.Entry(parameterFrame, textvariable = self.sdVar, width = 5)
		self.sdLabel = ttk.Label(parameterFrame, text = "stdev", width = 5)

		self.variables["distribution"] = self.distributionVar
		self.variables["minimum"] = self.minVar
		self.variables["maximum"] = self.maxVar
		self.variables["mean"] = self.meanVar
		self.variables["stdev"] = self.sdVar
		self.widgets = [self.labelWidget, self.typeWidget, self.distributionWidget, self.unitWidget, self.minLabel, self.minWidget, self.maxLabel, self.maxWidget, self.meanLabel, self.meanWidget, self.sdLabel, self.sdWidget]
		for widget in self.widgets:
			widget.bind("<MouseWheel>", ScrollParameter)
			widget.bind("<ButtonPress-4>", ScrollParameterDown)
			widget.bind("<ButtonPress-5>", ScrollParameterUp)		
	def PrepareForTypeChange(self, goal):
		if goal == "SimulaConstant":
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.text = "0#"
			self.XMLElement.attrib.pop("distribution", None)
			self.XMLElement.attrib.pop("minimum", None)
			self.XMLElement.attrib.pop("maximum", None)
			self.XMLElement.attrib.pop("mean", None)
			self.XMLElement.attrib.pop("stdev", None)
			self.XMLElement.tag = "SimulaConstant"
			return
		if goal == "SimulaTable":
			self.XMLElement.set("name_column2", self.labelText)
			self.XMLElement.set("unit_column2", self.unitVar.get())
			self.XMLElement.set("name_column1", "time since creation")
			self.XMLElement.set("unit_column1", "day")
			self.XMLElement.text = "0 0 100 0#"
			self.XMLElement.attrib.pop("name", None)
			self.XMLElement.attrib.pop("unit", None)
			self.XMLElement.attrib.pop("distribution", None)
			self.XMLElement.attrib.pop("minimum", None)
			self.XMLElement.attrib.pop("maximum", None)
			self.XMLElement.attrib.pop("mean", None)
			self.XMLElement.attrib.pop("stdev", None)
			self.XMLElement.tag = "SimulaTable"
			return
		if goal == "SimulaDerivative":
			self.XMLElement.set("function", "#FUNCTION#Choose a function#")
			self.XMLElement.attrib.pop("distribution", None)
			self.XMLElement.attrib.pop("minimum", None)
			self.XMLElement.attrib.pop("maximum", None)
			self.XMLElement.attrib.pop("mean", None)
			self.XMLElement.attrib.pop("stdev", None)
			self.XMLElement.tag = "SimulaDerivative"
			return
		if goal == "SimulaVariable":
			self.XMLElement.set("function", "#FUNCTION#Choose a function#")
			self.XMLElement.set("integrationFunction", "#FUNCTION#Choose an integration function#")
			self.XMLElement.attrib.pop("distribution", None)
			self.XMLElement.attrib.pop("minimum", None)
			self.XMLElement.attrib.pop("maximum", None)
			self.XMLElement.attrib.pop("mean", None)
			self.XMLElement.attrib.pop("stdev", None)
			self.XMLElement.tag = "SimulaVariable"
			return
		print("Error: Trying to change to unknown tag type.", self.labelText, goal)
	def CopyValuesIntoContainer(self, sourceElt):
		if "unit" in sourceElt.attrib:
			self.unitVar.set(sourceElt.get("unit"))
		if "distribution" in sourceElt.attrib:
			self.distributionVar.set(sourceElt.get("distribution"))
		if "minimum" in sourceElt.attrib:
			self.minVar.set(sourceElt.get("minimum"))
		if "maximum" in sourceElt.attrib:
			self.maxVar.set(sourceElt.get("maximum"))
		if "mean" in sourceElt.attrib:
			self.meanVar.set(sourceElt.get("mean"))
		if "stdev" in sourceElt.attrib:
			self.sdVar.set(sourceElt.get("stdev"))
	def SetPlaceHolder(self, entry):
		self.minVar.set(entry)
	
# Objects of this class contain SimulaDerivative elements
class DerivativeContainer(ContainerBase):
	def __init__(self, xmlElement, fileName, rClass):
		ContainerBase.__init__(self, xmlElement, fileName, rClass)
		self.labelText = xmlElement.get("name")
		if "GUITEXT" in xmlElement.attrib:
			self.labelText = xmlElement.get("GUITEXT")
		self.labelWidget = ttk.Label(parameterFrame, text = self.labelText, width = 25)
		self.typeVar.set(xmlElement.tag)
		self.typeWidget = ttk.Combobox(parameterFrame, textvariable = self.typeVar, state = "readonly", values = ["SimulaConstant", "SimulaTable", "SimulaStochastic", "SimulaDerivative", "SimulaVariable"], width = 14)
		self.typeWidget.bind('<<ComboboxSelected>>', ChangeElementTrigger)
		self.unitVar = StringVar()
		self.unitVar.set(xmlElement.get("unit"))
		if self.unitVar.get() == None:
			self.unitVar.set("None")
		self.unitWidget = ttk.Label(parameterFrame, text = self.unitVar.get())
		self.functionVar = StringVar()
		self.functionVar.set(xmlElement.get("function").split('#')[0])
		self.functionLabel = ttk.Label(parameterFrame, text = "Function", width = 8)
		self.functionWidget = ttk.Entry(parameterFrame, textvariable = self.functionVar, width = 20)
		self.variables["function"] = self.functionVar
		self.widgets = [self.labelWidget, self.typeWidget, self.functionLabel, self.functionWidget, self.unitWidget]
		for widget in self.widgets:
			widget.bind("<MouseWheel>", ScrollParameter)
			widget.bind("<ButtonPress-4>", ScrollParameterDown)
			widget.bind("<ButtonPress-5>", ScrollParameterUp)
	def PrepareForTypeChange(self, goal):
		if goal == "SimulaConstant":
			self.XMLElement.text = "0#"
			self.XMLElement.attrib.pop("function", None)
			self.XMLElement.tag = "SimulaConstant"
			return
		if goal == "SimulaTable":
			self.XMLElement.set("name_column2", self.labelText)
			self.XMLElement.set("unit_column2", self.unitVar.get())
			self.XMLElement.set("name_column1", "time since creation")
			self.XMLElement.set("unit_column1", "day")
			self.XMLElement.text = "0 0 100 0#"
			self.XMLElement.attrib.pop("name", None)
			self.XMLElement.attrib.pop("unit", None)
			self.XMLElement.attrib.pop("function", None)
			self.XMLElement.tag = "SimulaTable"
			return
		if goal == "SimulaStochastic":
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.set("distribution", "uniform#")
			self.XMLElement.set("minimum", "-0.05#")
			self.XMLElement.set("maximum", "0.05#")
			self.XMLElement.text = ""
			self.XMLElement.attrib.pop("function", None)
			self.XMLElement.tag = "SimulaStochastic"
			return
		if goal == "SimulaVariable":
			self.XMLElement.set("integrationFunction", "#FUNCTION#Choose an integration function#")
			self.XMLElement.tag = "SimulaVariable"
			return
		print("Error: Trying to change to unknown tag type.", self.labelText, goal)
	def CopyValuesIntoContainer(self, sourceElt):
		if "unit" in sourceElt.attrib:
			self.unitVar.set(sourceElt.get("unit"))
		if "function" in sourceElt.attrib:
			self.functionVar.set(sourceElt.get("function"))
	def SetPlaceHolder(self, entry):
		self.functionVar.set(entry)

# Objects of this class contain SimulaVariable elements
class VariableContainer(ContainerBase):
	def __init__(self, xmlElement, fileName, rClass):
		ContainerBase.__init__(self, xmlElement, fileName, rClass)
		self.labelText = xmlElement.get("name")
		if "GUITEXT" in xmlElement.attrib:
			self.labelText = xmlElement.get("GUITEXT")
		self.labelWidget = ttk.Label(parameterFrame, text = self.labelText, width = 25)
		self.typeVar.set(xmlElement.tag)
		self.typeWidget = ttk.Combobox(parameterFrame, textvariable = self.typeVar, state = "readonly", values = ["SimulaConstant", "SimulaTable", "SimulaStochastic", "SimulaDerivative", "SimulaVariable"], width = 14)
		self.typeWidget.bind('<<ComboboxSelected>>', ChangeElementTrigger)
		self.unitVar = StringVar()
		self.unitVar.set(xmlElement.get("unit"))
		if self.unitVar.get() == None:
			self.unitVar.set("None")
		self.unitWidget = ttk.Label(parameterFrame, text = self.unitVar.get())
		self.functionVar = StringVar()
		self.functionVar.set(xmlElement.get("function").split('#')[0])
		self.functionWidget = ttk.Entry(parameterFrame, textvariable = self.functionVar, width = 20)
		self.functionLabel = ttk.Label(parameterFrame, text = "Function", width = 8)
		self.integrationVar = StringVar()
		if not "integrationFunction" in xmlElement.attrib:
			xmlElement.set("integrationFunction", "#FUNCTION#Choose an integration function#")
		self.integrationVar.set(xmlElement.get("integrationFunction").split('#')[0])
		self.integrationWidget = ttk.Entry(parameterFrame, textvariable = self.integrationVar, width = 20)
		self.integrationLabel = ttk.Label(parameterFrame, text = "Integrationfunction", width = 19)
		self.variables["function"] = self.functionVar
		self.variables["integrationFunction"] = self.integrationVar
		self.widgets = [self.labelWidget, self.typeWidget, self.functionLabel, self.functionWidget, self.integrationLabel, self.integrationWidget, self.unitWidget]
		for widget in self.widgets:
			widget.bind("<MouseWheel>", ScrollParameter)
			widget.bind("<ButtonPress-4>", ScrollParameterDown)
			widget.bind("<ButtonPress-5>", ScrollParameterUp)
	def PrepareForTypeChange(self, goal):
		if goal == "SimulaConstant":
			self.XMLElement.text = "0#"
			self.XMLElement.attrib.pop("function", None)
			self.XMLElement.attrib.pop("integrationFunction", None)
			self.XMLElement.tag = "SimulaConstant"
			return
		if goal == "SimulaTable":
			self.XMLElement.set("name_column2", self.labelText)
			self.XMLElement.set("unit_column2", self.unitVar.get())
			self.XMLElement.set("name_column1", "time since creation")
			self.XMLElement.set("unit_column1", "day")
			self.XMLElement.text = "0 0 100 0#"
			self.XMLElement.attrib.pop("name", None)
			self.XMLElement.attrib.pop("unit", None)
			self.XMLElement.attrib.pop("function", None)
			self.XMLElement.attrib.pop("integrationFunction", None)
			self.XMLElement.tag = "SimulaTable"
			return
		if goal == "SimulaStochastic":
			self.XMLElement.set("unit", self.unitVar.get())
			self.XMLElement.set("distribution", "uniform#")
			self.XMLElement.set("minimum", "-0.05#")
			self.XMLElement.set("maximum", "0.05#")
			self.XMLElement.text = ""
			self.XMLElement.attrib.pop("function", None)
			self.XMLElement.attrib.pop("integrationFunction", None)
			self.XMLElement.tag = "SimulaStochastic"
			return
		if goal == "SimulaDerivative":
			self.XMLElement.attrib.pop("integrationFunction", None)
			self.XMLElement.tag = "SimulaDerivative"
			return
		print("Error: Trying to change to unknown tag type.", self.labelText, goal)
	def CopyValuesIntoContainer(self, sourceElt):
		if "unit" in sourceElt.attrib:
			self.unitVar.set(sourceElt.get("unit"))
		if "function" in sourceElt.attrib:
			self.functionVar.set(sourceElt.get("function"))
		if "integrationFunction" in sourceElt.attrib:
			self.integrationVar.set(sourceElt.get("integrationFunction"))
	def SetPlaceHolder(self, entry):
		self.functionVar.set(entry)
			
#keyDictionary = {"PLANTTYPE":str, "STRING":str, "COORDINATE":str, "INT":int, "DOUBLE":float, "BOOL":bool, "TABLE":str}

# Set up general lists that are used to store stuff
templates = []
with open("Configuration/Templates.csv", "r") as f:
	for line in f:
		templates.append(line.strip().rstrip())
	f.close()
templateCheckBoxList = []
currentTemplates = ["Branching"]
rootClasses = ["primaryRoot", "hypocotyl"]
parameterContainers = []
possibleDistributions = ["uniform", "normal"]
# Make a list with all template markers from a text file
templateMarkers = {}
with open("Configuration/TemplateMarkers.csv", "r") as f:
	for line in f:
		templateMarkers[line.partition(",")[0].strip()] = line.partition(",")[2].strip()
	f.close()

# Create main XML object and initialise root and branching templates
XMLTreeRoot = ET.Element("SimulationModel", {"name":"Model", "date":"xxx"})
XMLTree = ET.ElementTree(XMLTreeRoot)
rootTemplateRoot = ET.Element("SimulaDirective", {"path":"rootTypeParameters"})
rootTemplate = ET.ElementTree(rootTemplateRoot)
branchingTemplateRoot = ET.Element("SimulaBase")
branchingTemplate = ET.ElementTree(branchingTemplateRoot)
branchingTemplateString = ""
recordBranching = False
with open("Configuration/BranchingTemplate.xml", 'r') as f:
	for line in f:
		if "#BEGIN_TEMPLATE#" in line:
			recordBranching = True
			continue
		if "#END_TEMPLATE#" in line:
			recordBranching = False
		if recordBranching:
			branchingTemplateString += line
	f.close()
with tempfile.TemporaryFile() as f:
	f.write(bytes(branchingTemplateString, "UTF-8"))
	f.seek(0)
	branchingTemplate = ET.parse(f)
	branchingTemplateRoot = branchingTemplate.getroot()
	f.close()
whorlTemplateRoot = ET.Element("SimulaBase")
whorlTemplate = ET.ElementTree(whorlTemplateRoot)
whorlTemplateString = ""
recordWhorl = False
with open("Configuration/WhorlTemplate.xml", "r") as f:
	for line in f:
		if "#BEGIN_TEMPLATE#" in line:
			recordWhorl = True
			continue
		if "#END_TEMPLATE#" in line:
			recordWhorl = False
		if recordWhorl:
			whorlTemplateString += line
	f.close()
with tempfile.TemporaryFile() as f:
	f.write(bytes(whorlTemplateString, "UTF-8"))
	f.seek(0)
	whorlTemplate = ET.parse(f)
	whorlTemplateRoot = whorlTemplate.getroot()
	f.close()
legacyTillerTemplateRoot = ET.Element("SimulaBase")
legacyTillerTemplate = ET.ElementTree(legacyTillerTemplateRoot)
legacyTillerTemplateString = ""
recordLegacyTiller = False
with open("Configuration/LegacyTillerRootTemplate.xml", "r") as f:
	for line in f:
		if "#BEGIN_TEMPLATE#" in line:
			recordLegacyTiller = True
			continue
		if "#END_TEMPLATE#" in line:
			recordLegacyTiller = False
		if recordLegacyTiller:
			legacyTillerTemplateString += line
	f.close()
with tempfile.TemporaryFile() as f:
	f.write(bytes(legacyTillerTemplateString, "UTF-8"))
	f.seek(0)
	legacyTillerTemplate = ET.parse(f)
	legacyTillerTemplateRoot = legacyTillerTemplate.getroot()
	f.close()

# This function loads a template from a file and filters out the relevant parts to add to the etree
def LoadConfigurationFile(fileName):
	print("Loading: " + fileName)
	generalString = ""
	rootString = ""
	recordGeneral = False
	recordRoot = False
	with open("TemplateDescriptions/" + fileName, 'r') as f:
		for line in f:
			if "#OBLIGATORY_GENERAL#" in line:
				recordGeneral = True
				continue
			if "#END_OBLIGATORY_GENERAL#" in line:
				recordGeneral = False
			if recordGeneral:
				generalString += line
			if "#OBLIGATORY_ROOTS#" in line:
				recordRoot = True
				continue
			if "#END_OBLIGATORY_ROOTS#" in line:
				recordRoot = False
			if recordRoot:
				rootString += line
		f.close()
	with tempfile.TemporaryFile() as f:
		f.write(bytes(generalString, "UTF-8"))
		f.seek(0)
		generalTree = ET.parse(f)
		AddGeneralChoices(generalTree, fileName)
		f.close()
	if not rootString == "":
		with tempfile.TemporaryFile() as f:
			f.write(bytes(rootString, "UTF-8"))
			f.seek(0)
			rootTree = ET.parse(f)
			copyRootTree = deepcopy(rootTree)
			AddToRootTemplate(rootTree, fileName)
			AddToExistingRootClasses(copyRootTree, fileName)
			f.close()
# Should not add this root class when importing a file with tillers
	if fileName == "Descr_TillersLegacy.xml":
		currentTemplates.append("Descr_TillersLegacy.xml")
		AddRootClass("nodalrootsOfTillers", "hypocotyl", "legacyTiller")
# This function creates a dictionary linking each etree element to it's parent
def GetParentMap(etree):
	return {c:p for p in etree.iter() for c in p}
# This function tests if there is a choice in the tag text
def TestTextChoice(element):
	if element.text is None:
		return False
	if "#" in element.text:
		return True
# This function tests if there is a choice in the element attributes
def TestAttributeChoice(element):
	for value in element.attrib.values():
		if "#" in value:
			if value == "#":
				continue
			return True
	return False
# Checks if all dependencies are satisfied, not referenced anywhere so should probably remove
def CheckDependencies():
	returnValue = False
	global currentTemplates
	for entry in templateCheckBoxList:
		if entry[2].get() and len(entry[4]) > 0:
			for dependency in entry[4]:
				for probe in templateCheckBoxList:
					if not probe[2].get():
						probe[2].set(True)
						LoadConfigurationFile(probe[3])
						tempBool = CheckDependencies()
						if tempBool:
							returnValue = True
	if returnValue:
		currentTemplates = [el[3] for el in templateCheckBoxList if el[2].get()]
		currentTemplates.append("Branching")
		MergeSimulaDirectives(XMLTree)
		UpdateXMLTree(XMLTree)
		MergeSimulaDirectives(rootTemplate)
		UpdateXMLTree(rootTemplate)
	return returnValue
# Check if the current templates are consistent and adjust if needed
def CheckDependenciesClick(event):
	global currentTemplates
	for entry in templateCheckBoxList:
		if event.widget == entry[0]:
			if not entry[2].get():
				SetDependenciesTrue(entry)
				SetExclusionsFalse(entry)
				entry[2].set(True)
				continue
			if entry[2].get():
				print("Removing: " + entry[3])
				SetDependenciesFalse(entry)
				entry[2].set(False)
	for entry in templateCheckBoxList:
		if entry[2].get() and (entry[3] not in currentTemplates):
			LoadConfigurationFile(entry[3])
	currentTemplates = [el[3] for el in templateCheckBoxList if el[2].get()]
	currentTemplates.append("Branching")
	for entry in templateCheckBoxList:
		if event.widget == entry[0]:
			if not entry[2].get():
				entry[2].set(True)
				break
			if entry[2].get():
				entry[2].set(False)
				break
	MergeSimulaDirectives(XMLTree)
	UpdateXMLTree(XMLTree)
	MergeSimulaDirectives(rootTemplate)
	UpdateXMLTree(rootTemplate)
	UpdateParameterSection(rootClassTree.focus())
# Activates the templates needed by entry
def SetDependenciesTrue(entry):
	for dependency in entry[4]:
		if "OR" in dependency:
			tempList = dependency.split("OR")
			dependency = tempList[0]
			for alternative in tempList:
				for search in templateCheckBoxList:
					if search[3] == alternative:
						if search[2].get():
							dependency = alternative
		for search in templateCheckBoxList:
			if not search[2].get():
				if search[3] == dependency:
					search[2].set(True)
					SetDependenciesTrue(search)
# Deactivates the templates depending on entry
def SetDependenciesFalse(entry):
	for search in templateCheckBoxList:
		for dependency in search[4]:
			if entry[3] in dependency:
				if search[2].get():
					print("Removing: " + search[3])
					search[2].set(False)
					SetDependenciesFalse(search)
# Deactivates templates excluded by the activated entry
def SetExclusionsFalse(entry):
	for exclusion in entry[5]:
		for search in templateCheckBoxList:
			if search[3] == exclusion:
				if search[2].get():
					print("Removing: " + search[3])
					search[2].set(False)
# If this next line is uncommented, one can not switch between alternatives without deactivating templates that depend on this set of alternatives. 
# If there is a template that depends on a specific alternative out of a set, this line will need to be uncommented and a workaround needs to be found.
#					SetDependenciesFalse(search)
# Updates the XML try by removing unnecessary stuff	
def UpdateXMLTree(etree):
	global parameterContainers
	#currentTemplates = [el[3] for el in templateCheckBoxList if el[2]]
	popList = []
	for container in parameterContainers:
		if not container.file in currentTemplates:
			container.MakeInvisible()
	parameterContainers[:] = [container for container in parameterContainers if (container.file in currentTemplates)]
	parentMap = GetParentMap(etree)
	delList = []
	for el in etree.getroot().findall(".//"):
		if not el.get("GUIconfigFile") in currentTemplates:
			if not len(el):
				parentMap[el].remove(el)
				parentMap = GetParentMap(etree)
				continue
			el.set("GUIdelete", "1")
			delList.append(el)
			continue
		el.attrib.pop("GUIdelete", None)
	popList = []
	for el in delList:
		canBeDeleted = True
		for subel in el.findall(".//"):
			if not subel.get("GUIdelete") == "1":
				canBeDeleted = False
		if canBeDeleted:
			popList.append(el)
			parentMap[el].remove(el)
			continue
		el.attrib.pop("GUIdelete", None)
	for container in parameterContainers:
		if container.XMLElement in popList:
			container.MakeInvisible()
	parameterContainers[:] = [container for container in parameterContainers if (not container.XMLElement in popList)]
# Merges SimulaDirectives into their corresponding SimulaBase element	
def MergeSimulaDirectives(etree):
	searchingDepth = 1
	elements = [etree.getroot()]
	while len(elements) > 0:
		moveList = []
		elements = [etree.getroot()]
		for i in range(searchingDepth):
			elements = list(sum([el.findall("*") for el in elements], []))
		for el in elements:
			if el.tag == "SimulaDirective":
				findName = el.get("path").replace("/", "")
				parentMap = GetParentMap(etree)
				for search in parentMap[el].findall("*"):
					if search.get("name") == findName:
						moveList.append([el, search])
						break
		elements = [el for el in elements if not (el in [entry[0] for entry in moveList])]
		for el in moveList:
			for child in el[0].findall("*"):
				el[1].append(child)
				el[0].remove(child)
		searchingDepth += 1
	for i in range(searchingDepth):
		parentMap = GetParentMap(etree)
		SDList = etree.getroot().findall(".//SimulaDirective")
		for el in SDList:
			if len(el.findall("*")) == 0:
				parentMap[el].remove(el)
# Calls the updateparametersection function
def UpdateParameterSectionTrigger(event):
	UpdateParameterSection(rootClassTree.item(rootClassTree.identify_row(event.y), "text"))
	# Calls the updateparametersection function
def RefreshParameterSection(event):
	if expertMode.get():
		expertMode.set(False)
	else:
		expertMode.set(True)
	for container in parameterContainers:
		container.MakeInvisible()
	UpdateParameterSection(rootClassTree.focus())
	if expertMode.get():
		expertMode.set(False)
	else:
		expertMode.set(True)
# Hides and shows elements in the parameter section as appropriate
def UpdateParameterSection(currentClass):
	parameterLabel.set("Parameters of " + currentClass + ":")
	indices = [80 for i in range(len(templates))]
	branchingIndices = [10 for i in range(len(rootClasses))]
	for container in parameterContainers:
		if not container.rootClass == currentClass:
			container.MakeInvisible()
		if not container.file in currentTemplates:
			container.MakeInvisible()
		if container.rootClass == currentClass and container.file in currentTemplates:
			i = 0
			if container.file == "Branching":
				rootClassIndex = rootClasses.index(container.XMLElement.get("GUIBranchingClass"))
				if container.GUIPosition == None:
					i = 100*(len(templates) + 2) + 20*rootClassIndex + branchingIndices[rootClassIndex]
					branchingIndices[rootClassIndex] += 1
				else:
					i = 100*(len(templates) + 2) + 20*rootClassIndex + container.GUIPosition
				container.MakeVisible(i)
				continue
			templateIndex = templates.index(container.file)
			if container.GUIPosition == None:
				indices[templateIndex] += 1
				i = 100*templateIndex + 10 + indices[templateIndex]
			else:
				i = 100*templateIndex + 10 + container.GUIPosition
			container.MakeVisible(i)
			continue
	if currentClass == "Origin":
		ShowSwitchChange()
# Hides and shows options from the GUI as needed
def ShowSwitchChange():
	for container in parameterContainers:
		if "GUISHOWSWITCH" in container.XMLElement.attrib:
			for con in parameterContainers:
				if con.XMLElement.get("GUISHOW") == container.XMLElement.get("GUISHOWSWITCH"):
					if container.constVar.get() == "1":
						templateIndex = templates.index(con.file)
						i = 100*templateIndex + 10 + con.GUIPosition
						con.MakeVisible(i)
					else:
						con.MakeInvisible()
# Changes an element containing a choice to a different type
def ChangeElementTrigger(event):
	global parameterContainers
	currentWidget = event.widget
	goal = currentWidget.get()
	for container in parameterContainers:
		if currentWidget in container.widgets:
			if container.XMLElement.tag == goal:
				return
			container.PrepareForTypeChange(goal)
			AddElementToParameterDictionary(container.XMLElement, container.file, container.rootClass)
			container.MakeInvisible()
			parameterContainers[:] = [con for con in parameterContainers if (not con == container)]
			UpdateParameterSection(rootClassTree.focus())
# Adds a new root class
def AddToTreeView(event):
	newName = addToTreeEntry.get().strip()
	focus = rootClassTree.focus()
	if focus == "":
		messagebox.showinfo(message = "Select a root class to add this under!")
		return
	if newName == "Origin":
		messagebox.showinfo(message = "You can not name a root class 'Origin'!")
		return
	if newName == "primaryRoot":
		messagebox.showinfo(message = "You can not name a root class 'primaryRoot'!")
		return
	if newName == "hypocotyl":
		messagebox.showinfo(message = "You can not name a root class 'hypocotyl'!")
		return
	if newName == "":
		messagebox.showinfo(message = "Please type in a name for the new root class first!")
		return
	if focus == "Origin":
		messagebox.showinfo(message = "You can not add root classes to 'Origin'")
		return
	parent = rootClassTree.item(focus, "text")
	AddRootClass(newName, parent, "branch")
# Adds a new root whorl
def AddWhorlToTreeView(event):
	newName = addToTreeEntry.get().strip()
	focus = rootClassTree.focus()
	if focus == "":
		messagebox.showinfo(message = "Select a root class to add this under!")
		return
	if newName == "Origin":
		messagebox.showinfo(message = "You can not name a root class 'Origin'!")
		return
	if newName == "primaryRoot":
		messagebox.showinfo(message = "You can not name a root class 'primaryRoot'!")
		return
	if newName == "hypocotyl":
		messagebox.showinfo(message = "You can not name a root class 'hypocotyl'!")
		return
	if newName == "":
		messagebox.showinfo(message = "Please type in a name for the new root class first!")
		return
	if focus == "Origin":
		messagebox.showinfo(message = "You can not add root classes to 'Origin'")
		return
	parent = rootClassTree.item(focus, "text")
	AddRootClass(newName, parent, "whorl")
# Function that adds a new root class
def AddRootClass(newName, parent, type = "branch"):
	for entry in FindAllInstancesOfItemInRootClassTree(parent):
		newItem = rootClassTree.insert(entry, "end", text = newName, open = True)
	if not newName in rootClasses:
		rootClasses.append(newName)
		parameterCopyListBox.insert(END, newName)
		AddRootClassFromTemplate(newName)
	if type == "branch":
		AddBranchingParameters(newName, parent)
	if type == "whorl":
		AddWhorlParameters(newName, parent)
	if type == "legacyTiller":
		AddLegacyTillerParameters(newName, parent)
	MergeSimulaDirectives(XMLTree)
	UpdateXMLTree(XMLTree)
	UpdateParameterSection(rootClassTree.focus())
# Finds all entries in the tree view with a given name (text)
def FindAllInstancesOfItemInRootClassTree(item):
	returnList = []
	tempList = rootClassTree.get_children()
	while len(tempList) > 0:
		nextList = []
		for entry in tempList:
			if rootClassTree.item(entry, "text") == item:
				returnList.append(entry)
			nextList += rootClassTree.get_children(entry)
		tempList = nextList
	return returnList
# Deletes a lateral root class from the tree view
def DeleteFromTreeView():
	focusItem = rootClassTree.focus()
	focusName = rootClassTree.item(focusItem, "text")
	if focusItem == "Origin":
		messagebox.showinfo(message = "You can not delete the Origin!")
		return
	if focusItem == "Hypocotyl":
		messagebox.showinfo(message = "You can not delete the Hypocotyl!")
		return
	if focusItem == "PrimaryRoot":
		messagebox.showinfo(message = "You can not delete the PrimaryRoot!")
		return
	answer = messagebox.askyesno(message = "Are you sure you want to delete this entry and all its children? This can not be undone!", title = "Warning")
	if answer is True:
		DeleteRootClass(focusItem)
# Deletes a root class from the XML tree and parameterContainers list
def DeleteRootClass(focus):
	global XMLTreeRoot
	allItems = []
	currentItems = [rootClassTree.get_children()]
	while len(currentItems) > 0:
		allItems += currentItems
		tempList = currentItems
		currentItems = []
		for item in tempList:
			currentItems += rootClassTree.get_children(item)
	focusName = rootClassTree.item(focus, "text")
	stillExists = False
	for item in allItems:
		if item == focus:
			continue
		if rootClassTree.item(item, "text") == focusName:
			stillExists = True
			break
	for entryID in rootClassTree.get_children(focus):
		DeleteRootClass(entryID)
	parentClass = rootClassTree.item(rootClassTree.parent(focus), "text")
	MergeSimulaDirectives(XMLTree)
	UpdateXMLTree(XMLTree)
	parentInXML = XMLTreeRoot.findall("./SimulaBase[@name='rootTypeParameters']/SimulaBase[@name='#PLANTTYPE#']/SimulaBase[@name='" + parentClass + "']/SimulaBase[@name='branchList']/SimulaBase[@name='" + focusName + "']")[0]
	for container in parameterContainers:
		if container.XMLElement in parentInXML.findall(".//"):
			container.MakeInvisible()
	parameterContainers[:] = [container for container in parameterContainers if (not container.XMLElement in parentInXML.findall(".//"))]
	parentMap = GetParentMap(XMLTree)
	parentMap[parentInXML].remove(parentInXML)
	if not stillExists:	
		for i in range(0, parameterCopyListBox.size()):
			if parameterCopyListBox.get(i) == focusName:
				parameterCopyListBox.delete(i)
		parameterParent = XMLTreeRoot.findall("./SimulaBase[@name='rootTypeParameters']/SimulaBase[@name='#PLANTTYPE#']/SimulaBase[@name='" + focusName + "']")[0]
		for container in parameterContainers:
			if container.XMLElement in parameterParent.findall(".//"):
				container.MakeInvisible()
		parameterContainers[:] = [container for container in parameterContainers if (not container.XMLElement in parameterParent.findall(".//"))]
		XMLTreeRoot.findall("./SimulaBase[@name='rootTypeParameters']/SimulaBase[@name='#PLANTTYPE#']")[0].remove(parameterParent)
		rootClasses.remove(focusName)
	rootClassTree.delete(focus)
# Write parameters to the tags
def WriteParameters():
	plantType = ""
	for container in parameterContainers:
		if container.XMLElement.get("name") == "plantType":
			plantType = container.constVar.get()
	rootClassString = ""
	for rootClassName in rootClasses:
		rootClassString += rootClassName + ", "
	rootClassString = rootClassString[:-2]		
	for container in parameterContainers:
		if hasattr(container, "constVar"):
			container.XMLElement.text = container.constVar.get()
		for attr, var in container.variables.items():
			container.XMLElement.set(attr, var.get())
		if "name_column1" in container.XMLElement.attrib:
			if container.XMLElement.get("name_column1") == "time since creation":
				container.XMLElement.set("unit_column1", "day")
			if container.XMLElement.get("name_column1") == "depth":
				container.XMLElement.set("unit_column1", "cm")
	return [plantType, rootClassString]
# Export XML to file
def ExportXML():
	fileName = filedialog.asksaveasfilename()
	if not isinstance(fileName, str):
		print("Not a file!")
		return
	if fileName == "":
		print("No file chosen!")
		return
	print("Exporting to " + str(fileName))
	MergeSimulaDirectives(XMLTree)
	UpdateXMLTree(XMLTree)
	writeValues = WriteParameters()
	plantType = writeValues[0]
	rootClassString = writeValues[1]
	writeTree = deepcopy(XMLTree)
	for el in writeTree.findall(".//"):
		if el.get("name") == "#PLANTTYPE#":
			el.set("name", plantType)
		if not el.text is None:
			if "#ROOTCLASSES#" in el.text:
				el.text = rootClassString
	parentMap = GetParentMap(writeTree)
	labelList = writeTree.findall(".//Label")
	while len(labelList) > 0:
		parentMap[labelList[0]].remove(labelList[0])
		labelList = writeTree.findall(".//Label")
	removeList = []
	for el in writeTree.findall(".//"):
		if el.text == "BLANK":
			removeList.append(el)
			continue
		if "GUIConditionalOn" in el.attrib:
			for cond in el.get("GUIConditionalOn").split(';'):
				if not any(cond in templ for templ in currentTemplates):
					removeList.append(el)
					break
		for key, value in el.attrib.items():
			if value == "BLANK":
				removeList.append(el)
				break
	while len(removeList) > 0:
		parentMap[removeList[0]].remove(removeList[0])
		removeList.pop(0)
		parentMap = GetParentMap(writeTree)
	for el in writeTree.findall(".//"):
		popList = []
		for key in el.attrib.keys():
			if "GUI" in key:
				popList.append(key)
		for key in popList:
			el.attrib.pop(key, None)
	with tempfile.TemporaryFile() as f:
		writeTree.write(f)
		f.seek(0)
		cleanFile = ""
		for line in f:
			cleanFile += line.decode('utf-8', 'ignore')
		cleanFile = CleanFile(cleanFile)
		with open(fileName, 'wb') as g:
			g.write(cleanFile.encode('utf-8'))
			g.close()
		f.close()
	print("Done exporting!")
# To do. This will first clean up the xml given, then load it into memory, then detects which templates are active, using markers, then finds the root classes and adds them accordingly. Then the appropriate values are copied and the main xml tree is updated
def ImportXML():
	importXMLFile = filedialog.askopenfilename()
	if not isinstance(importXMLFile, str):
		return
	if len(importXMLFile) < 4:
		return
	print("Importing: ", importXMLFile)
# Clearing out XML tree
	global rootClasses
	for entry in rootClasses:
		if not (entry == "origin" or entry == "primaryRoot" or entry == "hypocotyl"):
			tempList = FindAllInstancesOfItemInRootClassTree(entry)
			for item in tempList:
				DeleteRootClass(item)
	global currentTemplates
	currentTemplates = ["Branching"]
	rootClasses = ["primaryRoot", "hypocotyl"]
	global XMLTreeRoot
	XMLTreeRoot = ET.Element("SimulationModel", {"name":"Model", "date":"xxx"})
	global XMLTree
	XMLTree = ET.ElementTree(XMLTreeRoot)
	global parameterContainers
	for container in parameterContainers:
		container.MakeInvisible()
	parameterContainers = []
	UpdateXMLTree(rootTemplate)
	fileString = ReadFile(importXMLFile)
	fileString = CleanZipFile(fileString, os.path.dirname(fileString))
	with tempfile.TemporaryFile() as f:
		f.write(fileString.strip().encode('utf-8'))
		f.seek(0)
		importTree = ET.parse(f)
		f.close()
	importTreeRoot = importTree.getroot()
	plantType = importTreeRoot.findall(".//SimulaConstant[@name='plantType']")[0].text.strip()
	print("Detected plant type = " + plantType)
	# Finding templates in import file and loading these into memory
	templatesInFile = []
	for key in templateMarkers.keys():
		if CheckForMarker(importTree, key, plantType):
			templatesInFile.append(key)
	print("The following modules were detected:", templatesInFile)
	for template in templatesInFile:
		if not template in currentTemplates:
			LoadConfigurationFile(template)
			for checkBox in templateCheckBoxList:
				if checkBox[3] == template:
					checkBox[2].set(True)
			currentTemplates.append(template)
	# Detect roots and rooting structure and add these accordingly
	rootList = importTreeRoot.findall("./SimulaBase[@name='rootTypeParameters']/SimulaBase[@name='" + plantType + "']")[0].findall("./SimulaBase")
	defaultsDictionary = {}
	for parentRoot in rootList:
		if len(parentRoot.findall("*[@name='copyDefaultsFrom']")) > 0:
			defaultsDictionary[parentRoot.get("name")] = parentRoot.findall("*[@name='copyDefaultsFrom']")[0].text.strip().rpartition('/')[2]
		if len(parentRoot.findall("./SimulaBase[@name='branchList']")) > 0:
			if len(parentRoot.findall("./SimulaBase[@name='branchList']")[0].findall("*")) > 0:
				for lateralRoot in parentRoot.findall("./SimulaBase[@name='branchList']")[0].findall("*"):
					if len(lateralRoot.findall("*[@name='numberOfBranches/whorl']")) > 0:
						print("Detected the root class " + lateralRoot.get("name") + " as whorl of " + parentRoot.get("name"))
						AddRootClass(lateralRoot.get("name"), parentRoot.get("name"), "whorl")
					elif len(lateralRoot.findall("*[@name='objectGeneratorClassToUse']")) > 0:
						print("Detected the root class " + lateralRoot.get("name") + " as special lateral (probably a tiller) of " + parentRoot.get("name"))
						continue
					else:
						print("Detected the root class " + lateralRoot.get("name") + " as lateral of " + parentRoot.get("name"))
						AddRootClass(lateralRoot.get("name"), parentRoot.get("name"), "branch")
	# Copy parameters from file that is being imported
	usingDefaults = False
	for container in parameterContainers:
		if container.typeVar.get() == "Label":
			continue
		path = GetElementPath(XMLTree, container.XMLElement)
		if "#PLANTTYPE#" in path:
			path[path.index("#PLANTTYPE#")] = plantType
		importElt = FindXMLElement(importTree, path)
		if isinstance(importElt, bool):
			printPath = deepcopy(path)
			if len(path) > 2:
				if not container.rootClass in defaultsDictionary:
					path[2] = "defaults"
				else:
					path[2] = defaultsDictionary[container.rootClass]
			importElt = FindXMLElement(importTree, path)
			if isinstance(importElt, bool):
				print("Did not find ", printPath, " and could not find default value to use.")
				container.SetPlaceHolder("BLANK")
			else:
				usingDefaults = True
		if not isinstance(importElt, bool):
			newContainer = container.PrepareForCopyingValues(importElt)
			newContainer.CopyValuesIntoContainer(importElt)
	if usingDefaults:
		print("Some values were not defined for some root classes. Default values found in the import file were used where possible. Make sure to check that values are correct.")
	UpdateXMLTree(XMLTree)
	UpdateXMLTree(rootTemplate)
	UpdateParameterSection(rootClassTree.focus())
	return
# Function that checks if the marker for a given template is in the XML tree
def CheckForMarker(etree, template, plantType):
	markerPath = templateMarkers[template].replace("#PLANTTYPE#", plantType).split("/")
	element = FindXMLElement(etree, markerPath)
	if not isinstance(element, bool):
		return True
	return False
# Print information on the XML element
def DisplayXMLElement(element):
	tempString = "Element of type " + element.tag + " the "
	if "name" in element.attrib:
		tempString += "name = " + element.get("name")
	if "path" in element.attrib:
		tempString += "path = " + element.get("path")
	if "name_column2" in element.attrib:
		tempString += "name_column2 = " + element.get("name_column2")
	print(tempString)
# Finds the element with path elementPath in the XML tree etree
def FindXMLElement(etree, elementPath):
	probeList = [etree.getroot()]
	for i in range(len(elementPath) - 1):
		tempList = probeList
		probeList = []
		for probe in tempList:
			probeList += GetXMLChildElement(probe, elementPath[i])
#		for probe in probeList:
#			print(probe.get("name"))
	foundList = []
	for probe in probeList:
		foundList += GetXMLBottomElement(probe, elementPath[len(elementPath)-1])
#	print(foundList)
	if len(foundList) > 1:
		print("Error: more than 1 element found for the path " + elementPath)
		return False
		print(foundList)
	if len(foundList) == 1:
		return foundList[0]
	if len(foundList) == 0:
		return False
# Finds a child element of a certain parent
def GetXMLChildElement(parent, child):
	possibilities = parent.findall("./SimulaBase[@name='" + child + "']")
	possibilities += parent.findall("./SimulaDirective[@path='" + child + "']")
	possibilities += parent.findall("./SimulaTable[@name_column2='" + child + "']")
	return possibilities
# Finds the final element in a path
def GetXMLBottomElement(parent, child):
	condition = ""
	if len(child.split(';')) > 1:
		condition = child.split(';')[1].split('=')[0]
		value = child.split(';')[1].split('=')[1]
		child = child.split(';')[0]
	if condition == "":
		possibilities = parent.findall("./*[@name='" + child + "']")
		possibilities += parent.findall("./*[@name_column2='" + child + "']")
	else:
		possibilities = parent.findall("./*[@name='" + child + "'][@" + condition + "='" + value + "']")
		possibilities += parent.findall("./*[@name_column2='" + child + "'][@" + condition + "='" + value + "']")
	if len(possibilities) > 1:
		print("Error: More than 1 bottom element found")
	return possibilities
# Constructs the element path for a givern element
def GetElementPath(etree, element):
	parentMap = GetParentMap(etree)
	path = []
	if "name" in element.attrib:
		path.insert(0, element.get("name"))
	if "name_column2" in element.attrib:
		path.insert(0, element.get("name_column2"))
	if "path" in element.attrib:
		path.insert(0, element.get("path"))
	parent = parentMap[element]
	while not parent == etree.getroot():
		if "name" in parent.attrib:
			path.insert(0, parent.get("name"))
		if "name_column2" in parent.attrib:
			path.insert(0, parent.get("name_column2"))
		if "path" in parent.attrib:
			path.insert(0, parent.get("path"))
		temp = parent
		parent = parentMap[temp]
	return path
# Displays a short tutorial and the license information
def DisplayAbout():
	ABOUT_TEXT = """This GUI creates XML input files for OpenSimRoot.
The latest version of this GUI can be found at: https://gitlab.com/rootmodels/OpenSimRootTools
The latest version of OpenSimRoot can be found at: https://gitlab.com/rootmodels/OpenSimRoot

First, select the modules you wish to include in your simulation using the tick boxes on the left. Dependencies are automatically taken care of.
Then use the buttons on the bottom left to add any root classes you wish to include (select the parent root class in the tree view, type the name of your new root class in the text box and click on the "Add" button).
Finally, fill in your parameters. The type of some parameters can be changed. If you are not sure what a parameter does, you are advised not to change it.
You can also import an existing XML file using the bottom found in the top left.
Once you are content with your changes, use the Export button to save your changes.

Only switch to expert mode if you are sure it is right for you. If this does not help you decide whether to use expert mode or not, don't use expert mode.

IMPORTANT: It is recommended that you only import one file per session and restart the GUI after exporting a file. This is to clear the memory and prevent any unforeseen interaction from causing issues. If you are constructing a file from scratch that includes tillers, make sure the Minimal Model template is loaded before you load the tiller template.

If you run into any issues, find bugs or otherwise unexpected behaviour or anything is unclear, please notify the developers by creating an issue in the repository: https://gitlab.com/rootmodels/OpenSimRootTools
"""
	LICENSE = """OpenSimRoot Tools - Copyright (C) - 2019 - Ernst Schäfer
All rights reserved.

This file is part of OpenSimRoot Tools.

OpenSimRoot Tools is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

OpenSimRoot Tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>."""
	toplevel = Toplevel()
	label1 = Label(toplevel, text=ABOUT_TEXT, height=20, width=150, wraplength=1100)
	label1.pack()
	label2 = Label(toplevel, text=LICENSE, height=20, width=150, wraplength=1100)
	label2.pack()
	toplevel.focus_force()
# To do, deletes everything and starts over. Not sure if this is useful
def ClearAll():
#	testRoot = ET.Element("SimulationModel", {"name":"Model", "date":"xxx"})
#	testTree = ET.ElementTree(testRoot)
#	testTree.write("testTree.xml")
	return
# To do. Copies parameters between root classes
def CopyParameters():
	return

##### MAIN #####
# TK stuff here that sets up the main window elements
root = Tk()
root.title("XML Generator")
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

mainFrame = ttk.Frame(root, padding="2")
#mainFrame.columnconfigure(0, weight=1)
#mainFrame.rowconfigure(0, weight=1)
#mainFrame.columnconfigure(1, weight=1)
mainFrame.rowconfigure(2, weight=1)
mainFrame.columnconfigure(2, weight=1)

mainFrame.grid(column=0, row=0, sticky=(N, W, E, S))

menuFrame = ttk.Frame(mainFrame, padding = "5", borderwidth = 2, relief = "solid")
templateFrameWrap = ttk.Labelframe(mainFrame, padding = "5", borderwidth = 2, relief = "solid", text="Choose which modules you want to use")
rootClassFrame = ttk.Labelframe(mainFrame, padding = "5", borderwidth = 2, relief = "solid", text="Set root classes")
parameterFrameWrap = ttk.Labelframe(mainFrame, padding = "5", borderwidth = 2, relief = "solid", text="Set parameters")


menuFrame.grid(column = 0, row = 0, sticky = (N, W, E, S), padx = 5, pady = 5)
templateFrameWrap.grid(column=0, row=1, sticky=(N, W, E, S), padx = 5, pady = 5)
rootClassFrame.grid(column=0, row=2, sticky=(N, W, E, S), padx = 5, pady = 5)
parameterFrameWrap.grid(column = 2, row = 0, sticky = (N, W, E, S), rowspan = 3, padx = 5, pady = 5)
sizingLabel = ttk.Label(mainFrame, text = "", width = 150).grid(column = 2, row = 3)

##### MENU #####

ttk.Label(menuFrame, text = "Welcome to the OSR XML inputfile generator GUI!").grid(column=0, row=0, sticky=(N, W, E), columnspan=3, padx = 10, pady = 10)
saveButton = ttk.Button(menuFrame, text = "Export to XML", command = ExportXML).grid(column=0, row=1, sticky=(N, W, E), padx = 10, pady = 10)
importButton = ttk.Button(menuFrame, text = "Import XML", command = ImportXML).grid(column=1, row=1, sticky=(N, W, E), padx = 10, pady = 10)
aboutButton = ttk.Button(menuFrame, text = "About", command = DisplayAbout).grid(column=2, row=1, sticky=(N, W, E), padx = 10, pady = 10)
#clearButton = ttk.Button(menuFrame, text = "Clear All", command = ClearAll).grid(column=2, row=1, sticky=(N, W, E), padx = 10, pady = 10)
expertMode = BooleanVar()
expertMode.set(False)
expertModeButton = ttk.Checkbutton(menuFrame, text = "Expert Mode", variable = expertMode)
expertModeButton.grid(column=3, row=1, sticky=(N, W, E), padx = 10, pady = 10)
expertModeButton.bind("<Button-1>", RefreshParameterSection)

##### TEMPLATES #####

templateCanvas = Canvas(templateFrameWrap, width = 600)
templateCanvas.pack(fill=BOTH, expand=YES)
templateFrame = ttk.Frame(templateCanvas, width = 200)
templateFrame.pack(fill=BOTH, expand=YES)
templateCanvas.create_window((0,0),window=templateFrame,anchor='nw')

templateScrollBar = ttk.Scrollbar(mainFrame, orient=VERTICAL, command=templateCanvas.yview)
templateScrollBar.grid(row=1, column=1, rowspan=1, sticky=(N, S))
templateCanvas["yscrollcommand"] = templateScrollBar.set
def ScrollFunction(event):
	templateCanvas.configure(scrollregion=templateCanvas.bbox("all"))
templateFrame.bind("<Configure>", ScrollFunction)
def ScrollTemplate(event):
	templateCanvas.yview_scroll(int(-event.delta/120), "units")
def ScrollTemplateDown(event):
	templateCanvas.yview_scroll(-1, "units")
def ScrollTemplateUp(event):
	templateCanvas.yview_scroll(1, "units")
templateCanvas.bind("<MouseWheel>", ScrollTemplate)
templateCanvas.bind("<ButtonPress-4>", ScrollTemplateDown)
templateCanvas.bind("<ButtonPress-5>", ScrollTemplateUp)
templateFrame.bind("<MouseWheel>", ScrollTemplate)
templateFrame.bind("<ButtonPress-4>", ScrollTemplateDown)
templateFrame.bind("<ButtonPress-5>", ScrollTemplateUp)

#for fileName in glob.glob("TemplateDescriptions/Descr_*"):
for fileName in templates:
	tempList = []
	with open("TemplateDescriptions/" + fileName, "r") as f:
		checkBoxBool = BooleanVar()
		templateDescription = ""
		checkBoxName = ""
		checkBoxDependencies = []
		checkBoxExclusions = []
		for line in f:
			if "#NAME#" in line:
				checkBoxName = line.partition("#NAME#")[2].partition("#")[0]
			if "#DESCRIPTION#" in line:
				templateDescription = line.partition("#DESCRIPTION#")[2].partition("#")[0]
			if "#DEPENDENCIES#" in line:
				checkBoxDependencies = line.partition("#DEPENDENCIES#")[2].split("#")
				checkBoxDependencies = checkBoxDependencies[:-1]
			if "#EXCLUDES#" in line:
				checkBoxExclusions = line.partition("#EXCLUDES#")[2].split("#")
				checkBoxExclusions = checkBoxExclusions[:-1]
#		tempList.extend((ttk.Checkbutton(templateFrame, text = checkBoxName, variable = checkBoxBool), ttk.Label(templateFrame, text = templateDescription), checkBoxBool, fileName.rpartition("/")[2], checkBoxDependencies))
		tempList.extend((ttk.Checkbutton(templateFrame, text = checkBoxName, variable = checkBoxBool), ttk.Label(templateFrame, text = templateDescription), checkBoxBool, fileName, checkBoxDependencies, checkBoxExclusions))
		f.close()
	templateCheckBoxList.append(tempList)
for i in range(len(templateCheckBoxList)):
	templateCheckBoxList[i][0].grid(column=0, row=i+1, sticky=(N, W, E), padx = 5, pady = 1)
	templateCheckBoxList[i][0].bind("<Button-1>", CheckDependenciesClick)
	templateCheckBoxList[i][1].grid(column=1, row=i+1, sticky=(N, W, E), padx = 5, pady = 1)
	templateCheckBoxList[i][2].set(False)
	for widget in templateCheckBoxList[i][:2]:
			widget.bind("<MouseWheel>", ScrollTemplate)
			widget.bind("<ButtonPress-4>", ScrollTemplateDown)
			widget.bind("<ButtonPress-5>", ScrollTemplateUp)
#templateCheckBoxList[2][0].unbind("<Button-1>")

##### ROOTCLASSES #####

rootClassTree = ttk.Treeview(rootClassFrame, height = 8)
rootClassTree.insert("", "end", "Origin", text="Origin", open=True)
rootClassTree.insert("Origin", "end", "primaryRoot", text="primaryRoot", open=True)
rootClassTree.insert("Origin", "end", "hypocotyl", text="hypocotyl", open=True)
rootClassTree.bind("<Button-1>", UpdateParameterSectionTrigger)

addToTreeName = StringVar()
addToTreeEntry = ttk.Entry(rootClassFrame, textvariable=addToTreeName, width = 15)
addToTreeEntry.bind("<Return>", AddToTreeView)
addToTreeButton = ttk.Button(rootClassFrame, text="Add")
addToTreeButton.bind("<Button-1>", AddToTreeView)
addWhorlButton = ttk.Button(rootClassFrame, text="Add Whorl")
addWhorlButton.bind("<Button-1>", AddWhorlToTreeView)
deleteFromTreeButton = ttk.Button(rootClassFrame, text="Delete", command = DeleteFromTreeView)
rootClassScrollBar = ttk.Scrollbar(rootClassFrame, orient=VERTICAL, command = rootClassTree.yview)
rootClassTree["yscrollcommand"] = rootClassScrollBar.set

rootClassTree.grid(column = 0, row = 0, sticky=(N, W, E), columnspan = 4)
addToTreeEntry.grid(column = 0, row = 1, sticky=(N, W, E, S), padx = 5, pady = 5)
addToTreeButton.grid(column = 1, row = 1, sticky=(N, W, E, S), padx = 5, pady = 5)
addWhorlButton.grid(column = 2, row = 1, sticky=(N, W, E, S), padx = 5, pady = 5)
deleteFromTreeButton.grid(column = 3, row = 1, sticky=(N, W, E, S), padx = 5, pady = 5)
rootClassScrollBar.grid(column = 4, row = 0, sticky=(N, S))

##### PARAMETERS #####

parameterCanvas = Canvas(parameterFrameWrap)
parameterCanvas.pack(fill=BOTH, expand=YES)
parameterFrame = ttk.Frame(parameterCanvas, width = 200)
parameterFrame.pack(fill=BOTH, expand=YES)
parameterCanvas.create_window((0,0),window=parameterFrame,anchor='nw')

parameterScrollBar = ttk.Scrollbar(mainFrame, orient=VERTICAL, command=parameterCanvas.yview)
parameterScrollBar.grid(row=0, column=3, rowspan=3, sticky=(N, S))
parameterCanvas["yscrollcommand"] = parameterScrollBar.set
def ScrollFunction(event):
	parameterCanvas.configure(scrollregion=parameterCanvas.bbox("all"))
parameterFrame.bind("<Configure>", ScrollFunction)
def ScrollParameter(event):
	parameterCanvas.yview_scroll(int(-event.delta/120), "units")
def ScrollParameterDown(event):
	parameterCanvas.yview_scroll(-1, "units")
def ScrollParameterUp(event):
	parameterCanvas.yview_scroll(1, "units")
parameterCanvas.bind("<MouseWheel>", ScrollParameter)
parameterCanvas.bind("<ButtonPress-4>", ScrollParameterDown)
parameterCanvas.bind("<ButtonPress-5>", ScrollParameterUp)
parameterFrame.bind("<MouseWheel>", ScrollParameter)
parameterFrame.bind("<ButtonPress-4>", ScrollParameterDown)
parameterFrame.bind("<ButtonPress-5>", ScrollParameterUp)

parameterLabel = StringVar()
parameterLabel.set("Parameters of :")
ttk.Label(parameterFrame, textvariable=parameterLabel).grid(column=0, row=2, sticky=(N, W, E), padx = 5, pady = 5, columnspan=2)
#ttk.Label(parameterFrame, text="Copy parameters from:", width = 25).grid(column=0, row=0, sticky=(N, W, E), padx = 5, pady = 5)
parameterCopyButton = ttk.Button(parameterFrame, text="Copy", command = CopyParameters)
parameterCopyVar = StringVar()
parameterCopyListBox = Listbox(parameterFrame, height=5, width = 25)
parameterCopyListBox.insert(END, "PrimaryRoot")
parameterCopyListBox.insert(END, "Hypocotyl")
parameterCopyListScrollBar = ttk.Scrollbar(parameterFrame, orient=VERTICAL, command=parameterCopyListBox.yview)
parameterCopyListBox["yscrollcommand"] = parameterCopyListScrollBar.set

#parameterCopyButton.grid(column=0, row=1, sticky=(N, W), padx = 5, pady = 5)
#parameterCopyListBox.grid(column=1, row=0, sticky=(N, W, E), padx = 5, pady = 5, rowspan = 2, columnspan = 2)
#parameterCopyListScrollBar.grid(column = 3, row = 0, rowspan = 2, sticky = (N, W, S))

# Adds the etree to the xml tree
def AddGeneralChoices(etree, fileName):
	AddToParameterDictionary(etree, fileName, "Origin")
	elList = etree.getroot().findall("*")
	for el in elList:
		XMLTreeRoot.append(el)
# Adds the etree to the rootTemplate xml structure
def AddToRootTemplate(etree, fileName):
	for el in etree.getroot().findall(".//"):
		el.set("GUIconfigFile", fileName)
	elList = etree.getroot().findall("*")
	for el in elList:
		rootTemplateRoot.append(el)
		etree.getroot().remove(el)		
# When creating a new root class, this function copies the xml structure from the rootTemplate
def AddRootClassFromTemplate(rootClass):
	treeCopy = deepcopy(rootTemplate)
	AddToParameterDictionary(treeCopy, "FromTemplate", rootClass)
	elList = treeCopy.getroot().findall("*")
	for el in elList:
		XMLTreeRoot.append(el)
	MergeSimulaDirectives(XMLTree)
	UpdateXMLTree(XMLTree)
# Adds the etree to all rootclasses that already exist
def AddToExistingRootClasses(etree, fileName):
	for rootClass in rootClasses:
		treeCopy = deepcopy(etree)
		AddToParameterDictionary(treeCopy, fileName, rootClass)
		elList = treeCopy.getroot().findall("*")
		for el in elList:
			XMLTreeRoot.append(el)
# Adds branching parameters to the etree and parameterContainers list
def AddBranchingParameters(rootClass, parentClass):
	templateCopy = deepcopy(branchingTemplate)
	for el in templateCopy.getroot().findall(".//"):
		el.set("GUIBranchingClass", rootClass)
		if el.tag == "Label":
			el.set("GUITEXT", el.get("GUITEXT").replace("#ROOTCLASS#", rootClass))
	templateCopy.getroot().set("name", rootClass)
	templateCopy.getroot().set("GUIconfigFile", "Branching")
	AddToParameterDictionary(templateCopy, "Branching", parentClass)
	XMLTreeRoot.findall("./SimulaBase[@name='rootTypeParameters']/SimulaBase[@name='#PLANTTYPE#']/SimulaBase[@name='" + parentClass + "']/SimulaBase[@name='branchList']")[0].append(templateCopy.getroot())
# Adds branching parameters to the etree and parameterContainers list
def AddWhorlParameters(rootClass, parentClass):
	templateCopy = deepcopy(whorlTemplate)
	for el in templateCopy.getroot().findall(".//"):
		el.set("GUIBranchingClass", rootClass)
		if el.tag == "Label":
			el.set("GUITEXT", el.get("GUITEXT").replace("#ROOTCLASS#", rootClass))
	templateCopy.getroot().set("name", rootClass)
	templateCopy.getroot().set("GUIconfigFile", "Branching")
	AddToParameterDictionary(templateCopy, "Branching", parentClass)
	XMLTreeRoot.findall("./SimulaBase[@name='rootTypeParameters']/SimulaBase[@name='#PLANTTYPE#']/SimulaBase[@name='" + parentClass + "']/SimulaBase[@name='branchList']")[0].append(templateCopy.getroot())
# Adds branching parameters to the etree and parameterContainers list
def AddLegacyTillerParameters(rootClass, parentClass):
	templateCopy = deepcopy(legacyTillerTemplate)
	for el in templateCopy.getroot().findall(".//"):
		el.set("GUIBranchingClass", rootClass)
		if el.tag == "Label":
			el.set("GUITEXT", el.get("GUITEXT").replace("#ROOTCLASS#", rootClass))
	templateCopy.getroot().set("name", rootClass)
	templateCopy.getroot().set("GUIconfigFile", "Branching")
	AddToParameterDictionary(templateCopy, "Branching", parentClass)
	XMLTreeRoot.findall("./SimulaBase[@name='rootTypeParameters']/SimulaBase[@name='#PLANTTYPE#']/SimulaBase[@name='" + parentClass + "']/SimulaBase[@name='branchList']")[0].append(templateCopy.getroot())
# Runs through an etree and adds elements to the parameterContainers list
def AddToParameterDictionary(etree, fileName, rootClass):
	for el in etree.getroot().findall(".//"):
		AddElementToParameterDictionary(el, fileName, rootClass)
# Adds an element to the parameterContainers list
def AddElementToParameterDictionary(el, fileName, rootClass):
	if fileName == "FromTemplate":
		fileName = el.get("GUIconfigFile")
	if not fileName == "FromTemplate":
		el.set("GUIconfigFile", fileName)
	el.set("GUIrootClass", rootClass)
	if (el.tag == "SimulaBase" and el.get("name") == "#ROOT#"):
		el.set("name", rootClass) 
		return
	if (el.tag == "SimulaDirective" and el.get("path") == "#ROOT#"):
		el.set("path", rootClass)
		return
	if (not el.text is None) and ("#ROOTCLASSES#" in el.text):
		return
	newContainer = ""
	if el.tag == "Label":
		newContainer = LabelContainer(el, fileName, rootClass)
	if el.tag == "SimulaConstant" and TestTextChoice(el):
		newContainer = ConstantContainer(el, fileName, rootClass)
	if el.tag == "SimulaTable" and TestTextChoice(el):
		newContainer = TableContainer(el, fileName, rootClass)
	if el.tag == "SimulaStochastic" and TestAttributeChoice(el):
		newContainer = StochasticContainer(el, fileName, rootClass)
	if el.tag == "SimulaDerivative" and TestAttributeChoice(el):
		newContainer = DerivativeContainer(el, fileName, rootClass)
	if el.tag == "SimulaVariable" and TestAttributeChoice(el):
		newContainer = VariableContainer(el, fileName, rootClass)
	if newContainer == "":
		return
	parameterContainers.append(newContainer)
	return newContainer
			
#templateCheckBoxList[2][2].set(True)
#templateCheckBoxList[2][0].state(["disabled"])
#LoadConfigurationFile(templateCheckBoxList[2][3])
MergeSimulaDirectives(XMLTree)
UpdateXMLTree(XMLTree)
MergeSimulaDirectives(rootTemplate)
UpdateXMLTree(rootTemplate)
rootClassTree.focus("Origin")
rootClassTree.selection_set("Origin")
UpdateParameterSection(rootClassTree.focus())

while True:
    try:
        root.mainloop()
        break
    except UnicodeDecodeError:
        pass










